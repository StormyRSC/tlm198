TheLoveMachine v2.0 #198
========================

TheLoveMachine was an attempt to write an open source bot for RuneScape Classic
in 2004 using the best known techniques of the time. It was written by
saevion and Sean for mudclient198 and later ported to mudclient201.
It was released to the rscheatnet community. Unfortunately, TLM never
achieved mass popularity due to its scripting interface being harder
to comprehend than its competitors.

Structurally, its design is quite similar to the (much later) APOS as a
stable, single-threaded extension bot - this design was probably inspired
by Evo's ixBot. Unlike APOS, it hooks a clean deobfuscated mudclient,
so might be a more educational example of how the RSC client and bots
work.

This is the 198 version, which is identical to the 201 version,
besides the packet opcodes.

Example usage
-------------

To start the client:

	ant run

Commands are IRC-style. All commands are scripts, including `/stop`.

Start autofighting:

	/fight 2 11

Every script defines a number of commands. Read the source code
of individual scripts to find the commands it defines.

TLM requires a fatigue OCR supporting the HC.BMP/slword.txt "standard",
such as LeoSleep or WUSS.

Changes by Stormy
-----------------

* Imported LoveFighter and CakeStealer scripts from rscheatnet.
* Fixed `loggedIn()`, `isMobFighting()`, `getPlayer()`.
* Added ant build system.
* Ported back to revision 198.
* Fixed font rendering on non-Windows platforms.
* Removed world selection via number-after-username. 
