/*

Ardongue cake thiever by - Unknown Idiot.
Start at cake stall in Ardonguw market (Members)
You sleeping bag, All other inv spots empty

*/

public class CakeStealer extends Script {

    public boolean InvFull = false;
    public boolean AtBank = false;
    public int Count = 0;
    public boolean WaitForBank = false;
    public String[] getCommands()
    {
        return new String[] {"cake"};
    }
    public boolean start(String macro, String params[])
    {
        rs.autosleep = true;
        rs.fatThreshold = 93;
        return true;
    }

    public long run(long ticks) 
    {
        if(isWalking()) return random(180,250);
        if(getNumInvItems() == 30) InvFull = true;
        if(getNumInvItem(330) == 0) InvFull = false;
        if(inArea(554,615,550,611)) AtBank = true;
        if(getX() == 543 && getY() == 600) AtBank = false;
        if(WaitForBank && !isBankOpen() && Count < 6000)
        {
            Count++;
            return 1;
        }

        if(getFatigue() > 93 || sleeping())
        {
            if(!sleeping())sleep();
            return random(280,350);
        } 

        if(getNumInvItems() < 30 && !InvFull && !AtBank)
        {
            int Stall = (getObjectAt(544,599));
            if(getObjectType(Stall) != 322) return 0;
            objectCmd2(Stall);
            return random(1000,1100);
        }

        if (!isWalking() && InvFull && !AtBank && getDistance(getX(),getY(), 552, 613)>=1)
        {
            walkTo(552, 613);
            return random(500,700);
        }

        if(AtBank && InvFull &&    !isBankOpen() && !isDialogShown())
        {
            for (int i = 0; i < getNumNpcs();i++)
            {
                int banker = -1;
                if (getNpcType(i)==95) banker = i;    
                if (banker != -1) talkToNpc(banker);
            }
        }
        if(AtBank && InvFull &&    !isBankOpen() && isDialogShown())
        {
            chooseDialogItem(0);
            Count = 0;
            return random(2000,3000);
        }
        if(AtBank && InvFull && isBankOpen())
        {
            depositItem(330);
            WaitForBank = false;
            return random(80,120);
        }
        if(AtBank && !InvFull && isBankOpen()) closeBankAcc();

        if (!isWalking() && !InvFull && AtBank && getDistance(getX(),getY(), 543, 600)>=1)
        {
            walkTo(543, 600);
            return random(500,700);
        }
        return random(500,700);
    }
}
