import java.util.ArrayList;
//originally by Br0ken
//Improved by bruncle: Anti Trap, walk back, unlimited number of pickup items

public class LoveFighter extends Script implements PrivateMessageListener,ChatMessageListener {
	public int npc_id,start_x,start_y,fight_mode;
	public ArrayList item_id = new ArrayList();
	public boolean clearWay;
	public String[] getCommands() {
		return new String[] {"fight"};
	}

	public boolean start(String cmd, String params[]) {
		rs.autosleep = true;
		rs.fatThreshold = 93;
		if (params.length > 0) {
			fight_mode = Integer.parseInt(params[0]);
			npc_id = Integer.parseInt(params[1]);
			if (params.length > 2)
				for (int i = 2; i < params.length; i++)
					item_id.add(new Integer(Integer.parseInt(params[i])));  
			start_x = getX(); start_y = getY(); 
			setCombatStyle(fight_mode);
			Println("Love Fighter started..");
			Println("Attacking "+getNpcName(npc_id));
			Println("Fight mode set on "+fight_mode);
			String items = "";
			for (int i = 0; i < item_id.size(); i++)
				items = items+ Integer.toString((Integer)item_id.get(i))+",";
			Println("Picking up "+items);
			System.out.println("Attacking " + npc_id + ", " + getNpcName(npc_id));
			return true;
		} else {
			System.out.println("You must specify a npc id to attack!");
			return false;
		}
	}

	public void Println(String msg){
		System.out.println(msg);
	}

	public long run(long ticks) {
		setCombatStyle(fight_mode);
		
		if(sleeping()){
			Println("Sleeping..");
			return 0;
		}
		
		if(getNumNpcs() <= 0) {
			return random(300,400);
		}
		
		if(getFatigueLevel() > 90) {
			System.out.println("Too fatigued, sleeping...");
			sleep();
			return random(500, 700);
		}
		
		for(int i = 0;i < getNumNpcs();i++) {
			if(getNpcType(i) == npc_id) {
				if(!isNpcInCombat(i)) {
					if (isReachable(getNpcPos(i).x,getNpcPos(i).y)) {
						attackNpc(i);
						return random(500, 700);
					} else {
						Println("Couldn't reach npc:(");
						clearWay = true;
					}
				}  
			}
		}
		
		for(int i = 0;i < getNumGroundItems();i++) {
			if(item_id.contains(new Integer(getGroundItemType(i)))) {
				if (isReachable(getGroundItemPos(i).x,getGroundItemPos(i).y)) {
					pickupGroundItem(i);
					return random(500, 700);
				} else {
					Println("Couldn't reach item:(");
					clearWay = true;
				}
			}
		}
		
		if (!inArea((start_x - 2),(start_y - 2),(start_x +2),(start_y+2))) {
			if (isReachable(start_x,start_y)) {
				if (walkTo(start_x,start_y))
					return random(500, 700);
			} else
				clearWay = true;
		}
		
		if (clearWay){
			clearWay = false;
			clearWay();
		}
		
		return random(500,700);
	}

	public void clearWay() {     
		Println("Trying to clear the way..");
		int i = 0;
		for (int j = 0; j < getNumObjects(); j++) {
			i = j;      
			if (getObjectDesc(getObjectType(i)).equalsIgnoreCase("The gate is shut")) {				
				System.out.println("Found a "+getObjectName(getObjectType(i))+" that needs opening..");
				if(objectCommandAt(getObjectPos(i).x,getObjectPos(i).y, "Open"))
					return;
			}
		}
		
		for (int j = 0; j < getNumWallObjects(); j++) {
			i = j;    
			if (getWallObjectDesc(getWallObjectType(i)).equalsIgnoreCase("The door is shut")) {
				System.out.println("Found a "+getWallObjectName(getWallObjectType(i))+" that needs opening..");
				wallObjectCmd1(i);
				return;    
			}      
		}
	}    

	public long runCombat(long ticks) {
		System.out.println("In combat...");
		setCombatStyle(fight_mode);
		return 100;
	}

	public void chatMessage(String message, String from) {
		System.out.println("Anti-mod chat activated : " + from + ": " + message);
		rs.stopScript("fight");
	}

	public void privateMessage(String message, String from) {
		System.out.println("Anti-mod pm activated : " + from + ": " + message);
		rs.stopScript("fight");
	}
}
